go-jwt
======

Introduction
------------

This is meant to be an implementation of JSON Web Token (JWT) written in Go.

[![Build Status](https://drone.io/bitbucket.org/osddk/go-jwt/status.png)](https://drone.io/bitbucket.org/osddk/go-jwt/latest)


Installation
------------

Install the package to your $GOPATH by using the go tool:

    $ go get bitbucket.org/osddk/go-jwt


Example
-----

TODO
