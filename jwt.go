// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

// JSON Web Token reding and parsing.
// See http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-20
package jwt

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

type Jwt struct {
	Issuer         string
	Subject        string
	Audience       string
	ExpirationTime time.Time
	NotBefore      time.Time
	IssuedAt       time.Time
	JwtId          string
	Private        map[string]interface{}
}

func (j *Jwt) UnmarshalJSON(data []byte) error {
	// Unmarshal json to map.
	m := make(map[string]interface{})
	if err := json.Unmarshal(data, &m); err != nil {
		return err
	}

	// Copy known claim name values to struct.
	// See http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-20#section-4.1
	if s, ok := m["iss"].(string); ok {
		j.Issuer = s
		delete(m, "iss")
	}

	if s, ok := m["sub"].(string); ok {
		j.Subject = s
		delete(m, "sub")
	}

	if s, ok := m["aud"].(string); ok {
		j.Audience = s
		delete(m, "aud")
	}

	if i, ok := m["exp"]; ok {
		switch t := i.(type) {
		case float64:
			j.ExpirationTime = time.Unix(int64(t), 0)
		case int64:
			j.ExpirationTime = time.Unix(t, 0)
		default:
			return fmt.Errorf("unable to parse exp as time: %#v", t)
		}
		delete(m, "exp")
	}

	if i, ok := m["nbf"]; ok {
		switch t := i.(type) {
		case float64:
			j.NotBefore = time.Unix(int64(t), 0)
		case int64:
			j.NotBefore = time.Unix(t, 0)
		default:
			return fmt.Errorf("unable to parse nbf as time: %#v", t)
		}
		delete(m, "nbf")
	}

	if i, ok := m["iat"]; ok {
		switch t := i.(type) {
		case float64:
			j.IssuedAt = time.Unix(int64(t), 0)
		case int64:
			j.IssuedAt = time.Unix(t, 0)
		default:
			return fmt.Errorf("unable to parse iat as time: %#v", t)
		}
		delete(m, "iat")
	}

	if s, ok := m["jti"].(string); ok {
		j.JwtId = s
		delete(m, "jti")
	}

	// Assume the rest are private claim names.
	j.Private = m

	return nil
}

// See http://tools.ietf.org/html/draft-ietf-jose-json-web-signature-26#section-4.1
type JwsHeader struct {
	Algorithm string `json:"alg"` // identifies the cryptographic algorithm used to secure the JWS
	KeyId     string `json:"kid,omitempty"`
	Type      string `json:"typ,omitempty"`
}

// See http://tools.ietf.org/html/draft-ietf-jose-json-web-encryption-26#section-4.1
type JweHeader struct {
	Algorithm  string `json:"alg"`
	Encryption string `json:"enc"`
	KeyId      string `json:"kid,omitempty"`
	Type       string `json:"typ,omitempty"`
}

func Parse(s string) (*Jwt, error) {
	// Split JWT parts and determine header type.
	// See http://tools.ietf.org/html/draft-ietf-jose-json-web-encryption-26#section-9
	p := strings.Split(s, ".")
	var h interface{}
	switch len(p) {
	case 3:
		h = &JwsHeader{}
	case 5:
		h = &JweHeader{}
	default:
		return nil, fmt.Errorf("Invalid JWT: expected 3 or 5 parts separated by periods; got %d", len(p))
	}

	// Decode header.
	d, err := decode(p[0])
	if err != nil {
		return nil, fmt.Errorf("Invalid JWT: failed decoding header: %s", err.Error())
	}

	// Parse json.
	if err := json.Unmarshal(d, &h); err != nil {
		return nil, fmt.Errorf("Invalid JWT: failed parsing json: %s", err.Error())
	}

	// TODO(taisph) Validate signature and handle encryption.

	// Decode payload.
	d, err = decode(p[1])
	if err != nil {
		return nil, fmt.Errorf("Invalid JWT: failed decoding payload: %s", err.Error())
	}

	// Parse json payload.
	j := new(Jwt)
	if err := json.Unmarshal(d, j); err != nil {
		return nil, fmt.Errorf("Invalid JWT: failed parsing json: %s", err.Error())
	}

	return j, nil
}

// Adds padding if necessary to base64url encoded string before decoding.
// See https://code.google.com/p/go/issues/detail?id=4237
func decode(s string) ([]byte, error) {
	padded := s
	if l := len(s) % 4; l > 0 {
		padded += string([]byte("===")[3-l:])
	}
	return base64.URLEncoding.DecodeString(padded)
}

// Encodes and strips padding as expected på JWT specs.
// See http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-20#section-2
func encode(src []byte) string {
	return strings.TrimRight(base64.URLEncoding.EncodeToString(src), "=")
}
