// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package jwt_test

import (
	. "bitbucket.org/osddk/go-jwt"
	"testing"
	"time"
)

const token = "eyJ0eXAiOiJKV1QiLA0KICJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGFtcGxlLmNvbS9pc19yb290Ijp0cnVlfQ.dBjftJeZ4CVP-mB92K27uhbUJU1p1r_wW1gFWFOEjXk"
const badfmttoken = "eyJ0eXAiOiJKV1QiLA0KICJhbGciOiJIUzI1NiJ9eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGFtcGxlLmNvbS9pc19yb290Ijp0cnVlfQdBjftJeZ4CVP-mB92K27uhbUJU1p1r_wW1gFWFOEjXk"
const badbase64token = "4hh25h2gr=eg3erh243gr5243gr23reg2er.87324hf783gf763gf7832hf8723hf783hf7=8h432f78h2389fh2389fh24389fh3w9efhw9fh89.23f902j3f0j2f0jwfjw90fq=ajw90f"
const badjsontoken = "4hh25h2greg3erh243gr5243gr23reg2er.87324hf783gf763gf7832hf8723hf783hf78h432f78h2389fh2389fh24389fh3w9efhw9fh89.23f902j3f0j2f0jwfjw90fqajw90f"

func TestParseSuccess(t *testing.T) {
	jwt, err := Parse(token)
	if err != nil {
		t.Errorf("error parsing token, got %v", err)
	}
	if jwt == nil {
		t.Errorf("parsing token should have returned a jwt, got %v", jwt)
	}
}

func TestParseFailFormat(t *testing.T) {
	jwt, err := Parse(badfmttoken)
	if err == nil {
		t.Errorf("parsing token with bad formatting should have returned error, got %v", jwt)
	}
	t.Log(err)
	if jwt != nil {
		t.Errorf("parsing token with bad formatting should not have returned a jwt, got %v", jwt)
	}
}

func TestParseFailDecoding(t *testing.T) {
	jwt, err := Parse(badbase64token)
	if err == nil {
		t.Errorf("parsing token with bad encoding should have returned error, got %v", jwt)
	}
	t.Log(err)
	if jwt != nil {
		t.Errorf("parsing token with bad encoding should not have returned a jwt, got %v", jwt)
	}
}

func TestParseFailJson(t *testing.T) {
	jwt, err := Parse(badjsontoken)
	if err == nil {
		t.Errorf("parsing token with bad json should have returned error, got %v", jwt)
	}
	t.Log(err)
	if jwt != nil {
		t.Errorf("parsing token with bad json should not have returned a jwt, got %v", jwt)
	}
}

func TestClaims(t *testing.T) {
	jwt, err := Parse(token)
	if err != nil {
		t.Errorf("error parsing token, got %v", err)
	}

	if iss := jwt.Issuer; iss != "joe" {
		t.Errorf("jwt.Issuer = %q, expected %q", iss, "joe")
	}

	if exp := jwt.ExpirationTime; !exp.Equal(time.Unix(1300819380, 0)) {
		t.Errorf("jwt.ExpirationTime = %q, expected %q", exp, time.Unix(1300819380, 0).String())
	}

	if private, ok := jwt.Private["http://example.com/is_root"].(bool); !ok || !private {
		t.Errorf("jwt.Private[\"http://example.com/is_root\"] = %q, expected %q", private, true)
	}
}
